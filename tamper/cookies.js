// ==UserScript==
// @name         GDPR - cookies (basic)
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @include      *
// @match        *
// @grant        GM_addStyle
// @grant        GM_getResourceText
// @resource     BOOTSTRAP_CSS  https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js
// @require      https://cdn.jsdelivr.net/npm/eosjs@16.0.8/lib/eos.min.js
// ==/UserScript==

// @require      http://mygdpr.info/script.js

// ==/UserScript==

var cssTxt = GM_getResourceText ("BOOTSTRAP_CSS");
GM_addStyle (cssTxt);

(function() {
    'use strict';
    $(document).ready(async function() {
        $('head').append(`
          <style>
#mygdpr {
height: 150px;
position: fixed;
bottom: 5px;
right: 10px;
z-index: 999999;
}

#gdpr_backdrop {
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  position: fixed;
  z-index: 999991;
  background-color: #000;
  opacity: 0.5;
}

#DIV_1 {
    bottom: 0px;
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    float: left;
    height: 150px;
    left: 0px;
    min-height: 1px;
    position: relative;
    right: 0px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    top: 0px;
    width: 839.25px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 419.625px 75px;
    transform-origin: 419.625px 75px;
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
    padding: 0px 15px;
}/*#DIV_1*/

#DIV_1:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_1:after*/

#DIV_1:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_1:before*/

#DIV_2 {
    box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 1px 0px;
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    height: 135px;
    min-height: 135px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    width: 809.25px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 404.625px 67.5px;
    transform-origin: 404.625px 67.5px;
    caret-color: rgb(51, 51, 51);
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(51, 51, 51);
    border-radius: 15px 15px 15px 15px;
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 0px 0px 15px;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_2*/

#DIV_2:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_2:after*/

#DIV_2:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_2:before*/

#DIV_3 {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    height: 0px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    width: 809.25px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 404.625px 0px;
    transform-origin: 404.625px 0px;
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_3*/

#DIV_3:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_3:after*/

#DIV_3:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_3:before*/

#DIV_4 {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    float: left;
    height: 68px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    width: 242.766px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 121.375px 34px;
    transform-origin: 121.375px 34px;
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 5px;
    outline: rgb(51, 51, 51) none 0px;
    padding: 5px;
}/*#DIV_4*/

#DIV_4:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_4:after*/

#DIV_4:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_4:before*/

#SPAN_5 {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 0px 0px;
    transform-origin: 0px 0px;
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#SPAN_5*/

#SPAN_5:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#SPAN_5:after*/

#SPAN_5:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#SPAN_5:before*/

#H3_6 {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    height: 26px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    width: 250px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 106.375px 13px;
    transform-origin: 106.375px 13px;
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 500 normal 24px / 26.4px "Source Sans Pro", sans-serif;
    margin: 6px 10px;
    outline: rgb(51, 51, 51) none 0px;
}/*#H3_6*/

#H3_6:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 500 normal 24px / 26.4px "Source Sans Pro", sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#H3_6:after*/

#H3_6:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 500 normal 24px / 26.4px "Source Sans Pro", sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#H3_6:before*/

#SPAN_7, #BR_13 {
    box-sizing: border-box;
    color: rgb(128, 128, 128);
    text-decoration: none solid rgb(128, 128, 128);
    text-size-adjust: 100%;
    column-rule-color: rgb(128, 128, 128);
    perspective-origin: 0px 0px;
    transform-origin: 0px 0px;
    caret-color: rgb(128, 128, 128);
    border: 0px none rgb(128, 128, 128);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(128, 128, 128) none 0px;
}/*#SPAN_7, #BR_13*/

#SPAN_7 {
    margin-left: 15px;
}

#SPAN_7:after, #BR_13:after {
    box-sizing: border-box;
    color: rgb(128, 128, 128);
    text-decoration: none solid rgb(128, 128, 128);
    text-size-adjust: 100%;
    column-rule-color: rgb(128, 128, 128);
    caret-color: rgb(128, 128, 128);
    border: 0px none rgb(128, 128, 128);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(128, 128, 128) none 0px;
}/*#SPAN_7:after, #BR_13:after*/

#SPAN_7:before, #BR_13:before {
    box-sizing: border-box;
    color: rgb(128, 128, 128);
    text-decoration: none solid rgb(128, 128, 128);
    text-size-adjust: 100%;
    column-rule-color: rgb(128, 128, 128);
    caret-color: rgb(128, 128, 128);
    border: 0px none rgb(128, 128, 128);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(128, 128, 128) none 0px;
}/*#SPAN_7:before, #BR_13:before*/

#DIV_8 {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    float: left;
    height: 70px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    width: 526px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 263px 35px;
    transform-origin: 263px 35px;
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 10px 5px 5px;
    outline: rgb(51, 51, 51) none 0px;
    padding: 5px;
}/*#DIV_8*/

#DIV_8:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_8:after*/

#DIV_8:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_8:before*/

#DIV_9 {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    float: left;
    height: 40px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    width: 809.25px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 404.625px 20px;
    transform-origin: 404.625px 20px;
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_9*/

#DIV_9:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_9:after*/

#DIV_9:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_9:before*/

#BUTTON_10 {
    color: rgb(255, 255, 255);
    cursor: pointer;
    display: block;
    float: left;
    height: 34px;
    min-height: 0px;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    touch-action: manipulation;
    vertical-align: middle;
    white-space: nowrap;
    width: 230px;
    column-rule-color: rgb(255, 255, 255);
    perspective-origin: 94.1875px 17px;
    transform-origin: 94.1875px 17px;
    user-select: none;
    caret-color: rgb(255, 255, 255);
    background: rgb(0, 0, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 1px solid rgb(54, 127, 169);
    border-radius: 15px 15px 15px 15px;
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 0px 0px 0px 15px;
    outline: rgb(255, 255, 255) none 0px;
    padding: 6px 12px;
}/*#BUTTON_10*/

#BUTTON_10:after {
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    cursor: pointer;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    white-space: nowrap;
    column-rule-color: rgb(255, 255, 255);
    user-select: none;
    caret-color: rgb(255, 255, 255);
    border: 0px none rgb(255, 255, 255);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(255, 255, 255) none 0px;
}/*#BUTTON_10:after*/

#BUTTON_10:before {
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    cursor: pointer;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    white-space: nowrap;
    column-rule-color: rgb(255, 255, 255);
    user-select: none;
    caret-color: rgb(255, 255, 255);
    border: 0px none rgb(255, 255, 255);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(255, 255, 255) none 0px;
}/*#BUTTON_10:before*/

#DIV_11 {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    float: right;
    height: 40px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    width: 400px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 167.734px 20px;
    transform-origin: 167.734px 20px;
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 0px 30px 0px 0px;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_11*/

#DIV_11:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_11:after*/

#DIV_11:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_11:before*/

#SPAN_12 {
    box-sizing: border-box;
    color: rgb(128, 128, 128);
    display: block;
    float: left;
    height: 40px;
    text-decoration: none solid rgb(128, 128, 128);
    text-size-adjust: 100%;
    width: 120px;
    column-rule-color: rgb(128, 128, 128);
    perspective-origin: 50px 20px;
    transform-origin: 50px 20px;
    caret-color: rgb(128, 128, 128);
    border: 0px none rgb(128, 128, 128);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(128, 128, 128) none 0px;
}/*#SPAN_12*/

#SPAN_12:after {
    box-sizing: border-box;
    color: rgb(128, 128, 128);
    text-decoration: none solid rgb(128, 128, 128);
    text-size-adjust: 100%;
    column-rule-color: rgb(128, 128, 128);
    caret-color: rgb(128, 128, 128);
    border: 0px none rgb(128, 128, 128);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(128, 128, 128) none 0px;
}/*#SPAN_12:after*/

#SPAN_12:before {
    box-sizing: border-box;
    color: rgb(128, 128, 128);
    text-decoration: none solid rgb(128, 128, 128);
    text-size-adjust: 100%;
    column-rule-color: rgb(128, 128, 128);
    caret-color: rgb(128, 128, 128);
    border: 0px none rgb(128, 128, 128);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(128, 128, 128) none 0px;
}/*#SPAN_12:before*/

#BUTTON_14 {
    color: rgb(255, 255, 255);
    cursor: pointer;
    display: block;
    float: left;
    height: 34px;
    min-height: 0px;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    touch-action: manipulation;
    vertical-align: middle;
    white-space: nowrap;
    width: 130px;
    column-rule-color: rgb(255, 255, 255);
    perspective-origin: 57.0781px 17px;
    transform-origin: 57.0781px 17px;
    user-select: none;
    caret-color: rgb(255, 255, 255);
    background: #ccc none repeat scroll 0% 0% / auto padding-box border-box;
    border: 1px solid #ccc;
    border-radius: 15px 15px 15px 15px;
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(255, 255, 255) none 0px;
    padding: 6px 12px;
}/*#BUTTON_14*/

#BUTTON_14:after {
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    cursor: pointer;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    white-space: nowrap;
    column-rule-color: rgb(255, 255, 255);
    user-select: none;
    caret-color: rgb(255, 255, 255);
    border: 0px none rgb(255, 255, 255);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(255, 255, 255) none 0px;
}/*#BUTTON_14:after*/

#BUTTON_14:before {
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    cursor: pointer;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    white-space: nowrap;
    column-rule-color: rgb(255, 255, 255);
    user-select: none;
    caret-color: rgb(255, 255, 255);
    border: 0px none rgb(255, 255, 255);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(255, 255, 255) none 0px;
}/*#BUTTON_14:before*/

#BUTTON_15 {
    color: rgb(255, 255, 255);
    cursor: pointer;
    display: block;
    float: left;
    height: 34px;
    min-height: 0px;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    touch-action: manipulation;
    vertical-align: middle;
    white-space: nowrap;
    width: 130px;
    column-rule-color: rgb(255, 255, 255);
    perspective-origin: 55.6562px 17px;
    transform-origin: 55.6562px 17px;
    user-select: none;
    caret-color: rgb(255, 255, 255);
    background: #ccc none repeat scroll 0% 0% / auto padding-box border-box;
    border: 1px solid #ccc;
    border-radius: 15px 15px 15px 15px;
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 0px 0px 0px 10px;
    outline: rgb(255, 255, 255) none 0px;
    padding: 6px 12px;
}/*#BUTTON_15*/

#BUTTON_15:after {
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    cursor: pointer;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    white-space: nowrap;
    column-rule-color: rgb(255, 255, 255);
    user-select: none;
    caret-color: rgb(255, 255, 255);
    border: 0px none rgb(255, 255, 255);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(255, 255, 255) none 0px;
}/*#BUTTON_15:after*/

#BUTTON_15:before {
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    cursor: pointer;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    white-space: nowrap;
    column-rule-color: rgb(255, 255, 255);
    user-select: none;
    caret-color: rgb(255, 255, 255);
    border: 0px none rgb(255, 255, 255);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(255, 255, 255) none 0px;
}/*#BUTTON_15:before*/




</style>
        `)

        $('body').append(`
<div id="gdpr_backdrop"></div>
<div id="mygdpr">
  <div id="DIV_2">
    <div id="DIV_3">
      <div id="DIV_4">
        <span id="SPAN_5"></span>
        <h3 id="H3_6">
          Your Privacy Choices
        </h3>
                <span id="SPAN_7">By cookiEOS</span>
      </div>
      <div id="DIV_8">
        We use cookies for a number of reasons, such as keeping FT Sites reliable and secure, personalising content and ads, providing social media features and to analyse how our Sites are used.
      </div>
      <div id="DIV_9">

        <button type="submit" value="Claim" id="BUTTON_10">
          Define Once for All Websites
        </button>
        <div id="DIV_11">
           <span id="SPAN_12">...or decide only<br id="BR_13" />for this website</span>
          <button type="submit" value="Claim" id="BUTTON_14">
            Accept Cookies
          </button>
          <button type="submit" value="Claim" id="BUTTON_15">
            Reject Cookies
          </button>
        </div>
      </div>
    </div>
    <!-- /.info-box-content -->

  </div>
</div>
        `)

        const contractaccname = "cookieacc"
        const account = "useraaaaaaaa"
        const privateKey = "5K7mtrinTFrVTduSxizUc5hjXJEtTjVTsqSHeBHes1Viep86FP5"

//         const eos = Eos({keyProvider: privateKey})
//         const result = await eos.transaction({
//             actions: [{
//                 account: contractaccname,
//                 name: "add",
//                 authorization: [{
//                     actor: account,
//                     permission: 'active',
//                 }],
//                 data: {
//                     account: account
//                 }
//             }],
//         });

//         console.log(result);

//         const eos = Eos({keyProvider: privateKey})
//         let result = await eos.transaction({
//             actions: [{
//                 account: contractaccname,
//                 name: "setcategory",
//                 authorization: [{
//                     actor: account,
//                     permission: 'active',
//                 }],
//                 data: {
//                     account: account,
//                     category: "sports",
//                     permission_level: 0
//                 }
//             }],
//         });

//         console.log(result);

//         result = await eos.transaction({
//             actions: [{
//                 account: contractaccname,
//                 name: "setcategory",
//                 authorization: [{
//                     actor: account,
//                     permission: 'active',
//                 }],
//                 data: {
//                     account: account,
//                     category: "news",
//                     permission_level: 3
//                 }
//             }],
//         });

//         console.log(result);





//         const eos = Eos({keyProvider: privateKey})
//         let result = await eos.transaction({
//             actions: [{
//                 account: contractaccname,
//                 name: "seturl",
//                 authorization: [{
//                     actor: account,
//                     permission: 'active',
//                 }],
//                 data: {
//                     account: account,
//                     url: "ft.com",
//                     permission_level: 2
//                 }
//             }],
//         });

//         console.log(result);

//         result = await eos.transaction({
//             actions: [{
//                 account: contractaccname,
//                 name: "seturl",
//                 authorization: [{
//                     actor: account,
//                     permission: 'active',
//                 }],
//                 data: {
//                     account: account,
//                     url: "bt.com",
//                     permission_level: 5
//                 }
//             }],
//         });

//         console.log(result);








    })

})();