// ==UserScript==
// @name         GDPR - bribe
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @include      *
// @match        *
// @grant        GM_addStyle
// @grant        GM_getResourceText
// @resource     BOOTSTRAP_CSS  https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js
// @require      https://cdn.jsdelivr.net/npm/eosjs@16.0.8/lib/eos.min.js
// ==/UserScript==

var cssTxt = GM_getResourceText ("BOOTSTRAP_CSS");
GM_addStyle (cssTxt);

(function() {
    'use strict';
    $(document).ready(async function() {
        $('head').append(`
          <style>
#mygdpr {
height: 290px;
position: fixed;
bottom: 5px;
right: 10px;
z-index: 999999;
}

#gdpr_backdrop {
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  position: fixed;
  z-index: 999991;
  background-color: #000;
  opacity: 0.5;
}

ul#UL_9 {
  list-style: none;
  margin-left: 10px;
}

#DIV_1 {
    box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 1px 0px;
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    height: 280px;
    min-height: 268px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    width: 348px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 162.656px 134px;
    transform-origin: 162.656px 134px;
    caret-color: rgb(51, 51, 51);
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(51, 51, 51);
    border-radius: 15px 15px 15px 15px;
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 0px 0px 15px;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_1*/

#DIV_1:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_1:after*/

#DIV_1:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_1:before*/

#DIV_2 {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    height: 85px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    width: 325.328px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 162.656px 42.5px;
    transform-origin: 162.656px 42.5px;
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_2*/

#DIV_2:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_2:after*/

#DIV_2:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_2:before*/

#DIV_3 {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    float: left;
    height: 74px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    width: 325.328px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 162.656px 37px;
    transform-origin: 162.656px 37px;
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 5px;
    outline: rgb(51, 51, 51) none 0px;
    padding: 5px;
}/*#DIV_3*/

#DIV_3:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_3:after*/

#DIV_3:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_3:before*/

#SPAN_4, #BR_8, #BR_11, #BR_13, #BR_15 {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 0px 0px;
    transform-origin: 0px 0px;
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#SPAN_4, #BR_8, #BR_11, #BR_13, #BR_15*/

#SPAN_4:after, #BR_8:after, #BR_11:after, #BR_13:after, #BR_15:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#SPAN_4:after, #BR_8:after, #BR_11:after, #BR_13:after, #BR_15:after*/

#SPAN_4:before, #BR_8:before, #BR_11:before, #BR_13:before, #BR_15:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#SPAN_4:before, #BR_8:before, #BR_11:before, #BR_13:before, #BR_15:before*/

#H3_5 {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    height: 52px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    width: 295.328px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 147.656px 26px;
    transform-origin: 147.656px 26px;
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 500 normal 24px / 26.4px "Source Sans Pro", sans-serif;
    margin: 6px 10px;
    outline: rgb(51, 51, 51) none 0px;
}/*#H3_5*/

#H3_5:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 500 normal 24px / 26.4px "Source Sans Pro", sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#H3_5:after*/

#H3_5:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 500 normal 24px / 26.4px "Source Sans Pro", sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#H3_5:before*/

#HR_6 {
    color: rgb(51, 51, 51);
    height: 0px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    width: 325.328px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 162.656px 0.5px;
    transform-origin: 162.656px 0.5px;
    caret-color: rgb(51, 51, 51);
    border-top: 1px solid rgb(238, 238, 238);
    border-right: 0px none rgb(51, 51, 51);
    border-bottom: 0px solid rgb(51, 51, 51);
    border-left: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 12px 0px;
    outline: rgb(51, 51, 51) none 0px;
}/*#HR_6*/

#HR_6:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#HR_6:after*/

#HR_6:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#HR_6:before*/

#DIV_7 {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    float: left;
    height: 110px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    width: 312.312px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 156.156px 55px;
    transform-origin: 156.156px 55px;
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 0px 10px 10px;
    outline: rgb(51, 51, 51) none 0px;
    padding: 0px 5px;
}/*#DIV_7*/

#DIV_7:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_7:after*/

#DIV_7:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_7:before*/

#UL_9 {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    height: 60px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    width: 302.312px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 151.156px 30px;
    transform-origin: 151.156px 30px;
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 0px 0px 10px;
    outline: rgb(51, 51, 51) none 0px;
    padding: 0px;
}/*#UL_9*/

#UL_9:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#UL_9:after*/

#UL_9:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#UL_9:before*/

#I_10, #I_12 {
    bottom: -1px;
    box-sizing: border-box;
    color: rgb(0, 128, 0);
    display: inline-block;
    height: 14px;
    left: 0px;
    position: relative;
    right: 0px;
    text-decoration: none solid rgb(0, 128, 0);
    text-size-adjust: 100%;
    top: 1px;
    width: 14px;
    column-rule-color: rgb(0, 128, 0);
    perspective-origin: 7px 7px;
    transform-origin: 7px 7px;
    caret-color: rgb(0, 128, 0);
    border: 0px none rgb(0, 128, 0);
    font: normal normal 400 normal 14px / 14px "Glyphicons Halflings";
    margin: 0px 3px 0px 0px;
    outline: rgb(0, 128, 0) none 0px;
}/*#I_10, #I_12*/

#I_10:after, #I_12:after {
    box-sizing: border-box;
    color: rgb(0, 128, 0);
    text-decoration: none solid rgb(0, 128, 0);
    text-size-adjust: 100%;
    column-rule-color: rgb(0, 128, 0);
    caret-color: rgb(0, 128, 0);
    border: 0px none rgb(0, 128, 0);
    font: normal normal 400 normal 14px / 14px "Glyphicons Halflings";
    outline: rgb(0, 128, 0) none 0px;
}/*#I_10:after, #I_12:after*/

#I_10:before, #I_12:before {
    box-sizing: border-box;
    color: rgb(0, 128, 0);
    content: '""';
    text-decoration: none solid rgb(0, 128, 0);
    text-size-adjust: 100%;
    column-rule-color: rgb(0, 128, 0);
    perspective-origin: 0px 0px;
    transform-origin: 0px 0px;
    caret-color: rgb(0, 128, 0);
    border: 0px none rgb(0, 128, 0);
    font: normal normal 400 normal 14px / 14px "Glyphicons Halflings";
    outline: rgb(0, 128, 0) none 0px;
}/*#I_10:before, #I_12:before*/

#I_14 {
    bottom: -1px;
    box-sizing: border-box;
    color: rgb(0, 128, 0);
    display: inline-block;
    height: 14px;
    left: 0px;
    position: relative;
    right: 0px;
    text-decoration: none solid rgb(0, 128, 0);
    text-size-adjust: 100%;
    top: 1px;
    width: 14px;
    column-rule-color: rgb(0, 128, 0);
    perspective-origin: 7px 7px;
    transform-origin: 7px 7px;
    caret-color: rgb(0, 128, 0);
    border: 0px none rgb(0, 128, 0);
    font: normal normal 400 normal 14px / 14px "Glyphicons Halflings";
    margin: 0px 3px 0px 0px;
    outline: rgb(0, 128, 0) none 0px;
}/*#I_14*/

#I_14:after {
    box-sizing: border-box;
    color: rgb(0, 128, 0);
    text-decoration: none solid rgb(0, 128, 0);
    text-size-adjust: 100%;
    column-rule-color: rgb(0, 128, 0);
    caret-color: rgb(0, 128, 0);
    border: 0px none rgb(0, 128, 0);
    font: normal normal 400 normal 14px / 14px "Glyphicons Halflings";
    outline: rgb(0, 128, 0) none 0px;
}/*#I_14:after*/

#I_14:before {
    box-sizing: border-box;
    color: rgb(0, 128, 0);
    content: '""';
    text-decoration: none solid rgb(0, 128, 0);
    text-size-adjust: 100%;
    column-rule-color: rgb(0, 128, 0);
    perspective-origin: 0px 0px;
    transform-origin: 0px 0px;
    caret-color: rgb(0, 128, 0);
    border: 0px none rgb(0, 128, 0);
    font: normal normal 400 normal 14px / 14px "Glyphicons Halflings";
    outline: rgb(0, 128, 0) none 0px;
}/*#I_14:before*/

#SPAN_16 {
    box-sizing: border-box;
    color: rgb(128, 128, 128);
    display: block;
    float: right;
    height: 20px;
    text-decoration: none solid rgb(128, 128, 128);
    text-size-adjust: 100%;
    width: 75px;
    column-rule-color: rgb(128, 128, 128);
    perspective-origin: 35.2656px 10px;
    transform-origin: 35.2656px 10px;
    caret-color: rgb(128, 128, 128);
    border: 0px none rgb(128, 128, 128);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(128, 128, 128) none 0px;
}/*#SPAN_16*/

#SPAN_16:after {
    box-sizing: border-box;
    color: rgb(128, 128, 128);
    text-decoration: none solid rgb(128, 128, 128);
    text-size-adjust: 100%;
    column-rule-color: rgb(128, 128, 128);
    caret-color: rgb(128, 128, 128);
    border: 0px none rgb(128, 128, 128);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(128, 128, 128) none 0px;
}/*#SPAN_16:after*/

#SPAN_16:before {
    box-sizing: border-box;
    color: rgb(128, 128, 128);
    text-decoration: none solid rgb(128, 128, 128);
    text-size-adjust: 100%;
    column-rule-color: rgb(128, 128, 128);
    caret-color: rgb(128, 128, 128);
    border: 0px none rgb(128, 128, 128);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(128, 128, 128) none 0px;
}/*#SPAN_16:before*/

#DIV_17 {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    float: left;
    height: 34px;
    max-height: 34px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    width: 310px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 146.391px 17px;
    transform-origin: 146.391px 17px;
    caret-color: rgb(51, 51, 51);
    background: rgb(211, 211, 211) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(51, 51, 51);
    border-radius: 18px 18px 18px 18px;
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 0px 0px 0px 20px;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_17*/

#DIV_17:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_17:after*/

#DIV_17:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_17:before*/

#BUTTON_18 {
    box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
    color: rgb(255, 255, 255);
    cursor: pointer;
    display: block;
    float: left;
    height: 34px;
    min-height: 0px;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    touch-action: manipulation;
    vertical-align: middle;
    white-space: nowrap;
    width: 125px;
    column-rule-color: rgb(255, 255, 255);
    perspective-origin: 58.875px 17px;
    transform-origin: 58.875px 17px;
    user-select: none;
    caret-color: rgb(255, 255, 255);
    background: rgb(0, 0, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 1px solid rgb(54, 127, 169);
    border-radius: 15px 15px 15px 15px;
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(255, 255, 255) none 0px;
    padding: 6px 12px;
}/*#BUTTON_18*/

#BUTTON_18:after {
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    cursor: pointer;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    white-space: nowrap;
    column-rule-color: rgb(255, 255, 255);
    user-select: none;
    caret-color: rgb(255, 255, 255);
    border: 0px none rgb(255, 255, 255);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(255, 255, 255) none 0px;
}/*#BUTTON_18:after*/

#BUTTON_18:before {
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    cursor: pointer;
    text-decoration: none solid rgb(255, 255, 255);
    text-size-adjust: 100%;
    white-space: nowrap;
    column-rule-color: rgb(255, 255, 255);
    user-select: none;
    caret-color: rgb(255, 255, 255);
    border: 0px none rgb(255, 255, 255);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(255, 255, 255) none 0px;
}/*#BUTTON_18:before*/

#DIV_19 {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    float: left;
    height: 20px;
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    width: 157px;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 68.2344px 10px;
    transform-origin: 68.2344px 10px;
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 7px 7px 7px 20px;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_19*/

#DIV_19:after {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_19:after*/

#DIV_19:before {
    box-sizing: border-box;
    color: rgb(51, 51, 51);
    text-decoration: none solid rgb(51, 51, 51);
    text-size-adjust: 100%;
    column-rule-color: rgb(51, 51, 51);
    caret-color: rgb(51, 51, 51);
    border: 0px none rgb(51, 51, 51);
    font: normal normal 400 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
}/*#DIV_19:before*/

#SPAN_20 {
    box-sizing: border-box;
    color: rgb(255, 0, 0);
    text-decoration: none solid rgb(255, 0, 0);
    text-size-adjust: 100%;
    column-rule-color: rgb(255, 0, 0);
    perspective-origin: 0px 0px;
    transform-origin: 0px 0px;
    caret-color: rgb(255, 0, 0);
    border: 0px none rgb(255, 0, 0);
    font: normal normal 700 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 0px 0px 0px 3px;
    outline: rgb(255, 0, 0) none 0px;
}/*#SPAN_20*/

#SPAN_20:after {
    box-sizing: border-box;
    color: rgb(255, 0, 0);
    text-decoration: none solid rgb(255, 0, 0);
    text-size-adjust: 100%;
    column-rule-color: rgb(255, 0, 0);
    caret-color: rgb(255, 0, 0);
    border: 0px none rgb(255, 0, 0);
    font: normal normal 700 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(255, 0, 0) none 0px;
}/*#SPAN_20:after*/

#SPAN_20:before {
    box-sizing: border-box;
    color: rgb(255, 0, 0);
    text-decoration: none solid rgb(255, 0, 0);
    text-size-adjust: 100%;
    column-rule-color: rgb(255, 0, 0);
    caret-color: rgb(255, 0, 0);
    border: 0px none rgb(255, 0, 0);
    font: normal normal 700 normal 14px / 20px "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    outline: rgb(255, 0, 0) none 0px;
}/*#SPAN_20:before*/


.congrats {
  display: none;
  position: fixed;
  padding: 10px;
  right: 40px;
  bottom: 20px;
  width: 270px;
  height: 120px;
  background-color: white;
  border-radius: 10px;
  box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 1px 0px;
  z-index: 9999999;
}

.close {
  margin-right: 11px;
  margin-top: 5px;
}

</style>
        `)

        $('body').append(`
<div id="gdpr_backdrop"></div>
<div id="mygdpr">
<span class="close">x</span>
<div id="DIV_1">
  <div id="DIV_2">
    <div id="DIV_3">
      <span id="SPAN_4"></span>
      <h3 id="H3_5">
        We offer you 5 coins to be able to monetize your data
      </h3>
    </div><hr id="HR_6" />
    <div id="DIV_7">
      You would agree for min of 6 months to:<br id="BR_8" />
      <ul id="UL_9">
        <li>
                   <svg xmlns="http://www.w3.org/2000/svg" width="13" height="10" viewBox="0 0 13 10"><path fill="#97DF4F" fill-rule="nonzero" d="M11.304 0L13 1.765 5.098 10 0 4.706 1.696 2.94l3.402 3.53z"/></svg>
                   Accept cookies</li>
                <li>
                   <svg xmlns="http://www.w3.org/2000/svg" width="13" height="10" viewBox="0 0 13 10"><path fill="#97DF4F" fill-rule="nonzero" d="M11.304 0L13 1.765 5.098 10 0 4.706 1.696 2.94l3.402 3.53z"/></svg>
                   It's OK to use my data internally<br id="BR_13" />
                <li>
                   <svg xmlns="http://www.w3.org/2000/svg" width="13" height="10" viewBox="0 0 13 10"><path fill="#97DF4F" fill-rule="nonzero" d="M11.304 0L13 1.765 5.098 10 0 4.706 1.696 2.94l3.402 3.53z"/></svg>
                   Its OK to share with third parties anonymously</li>
      </ul> <span id="SPAN_16">learn more..</span>
    </div>
        <hr id="HR_6">
    <div id="DIV_17">
      <button type="submit" value="Claim" id="BUTTON_18">
        Add to Whitelist
      </button>
      <div id="DIV_19">
        in exchange for <span id="SPAN_20">5 coins</span>
      </div>
    </div>
  </div>
  <!-- /.info-box-content -->
</div>

</div>
        `)


        $('body').append(`
<div class="congrats">
<img src="https://imgur.com/download/dxTz2MG" style="float: left">
<h2 style="padding-left: 110px;">Thanks!</h2>
<p style="padding-left: 110px;">You earned 5 coins!</p>
</div>
`)


$("#mygdpr .close").click(function() {
  $("#mygdpr").fadeOut()
})

        $('#BUTTON_18').click(function() {
$("#DIV_1").fadeOut(function() {
  $(this).next();
  $(".congrats").fadeIn().next()
  window.setTimeout(function(){$(".congrats").fadeOut()}, 5000)
})
})

        const contractaccname = "cookieacc"
        const account = "useraaaaaaaa"
        const privateKey = "5K7mtrinTFrVTduSxizUc5hjXJEtTjVTsqSHeBHes1Viep86FP5"

//         const eos = Eos({keyProvider: privateKey})
//         const result = await eos.transaction({
//             actions: [{
//                 account: contractaccname,
//                 name: "add",
//                 authorization: [{
//                     actor: account,
//                     permission: 'active',
//                 }],
//                 data: {
//                     account: account
//                 }
//             }],
//         });

//         console.log(result);

//         const eos = Eos({keyProvider: privateKey})
//         let result = await eos.transaction({
//             actions: [{
//                 account: contractaccname,
//                 name: "setcategory",
//                 authorization: [{
//                     actor: account,
//                     permission: 'active',
//                 }],
//                 data: {
//                     account: account,
//                     category: "sports",
//                     permission_level: 0
//                 }
//             }],
//         });

//         console.log(result);

//         result = await eos.transaction({
//             actions: [{
//                 account: contractaccname,
//                 name: "setcategory",
//                 authorization: [{
//                     actor: account,
//                     permission: 'active',
//                 }],
//                 data: {
//                     account: account,
//                     category: "news",
//                     permission_level: 3
//                 }
//             }],
//         });

//         console.log(result);





//         const eos = Eos({keyProvider: privateKey})
//         let result = await eos.transaction({
//             actions: [{
//                 account: contractaccname,
//                 name: "seturl",
//                 authorization: [{
//                     actor: account,
//                     permission: 'active',
//                 }],
//                 data: {
//                     account: account,
//                     url: "ft.com",
//                     permission_level: 2
//                 }
//             }],
//         });

//         console.log(result);

//         result = await eos.transaction({
//             actions: [{
//                 account: contractaccname,
//                 name: "seturl",
//                 authorization: [{
//                     actor: account,
//                     permission: 'active',
//                 }],
//                 data: {
//                     account: account,
//                     url: "bt.com",
//                     permission_level: 5
//                 }
//             }],
//         });

//         console.log(result);








    })

})();