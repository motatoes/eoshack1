#include <eosiolib/eosio.hpp>
#include <eosio.token/eosio.token.hpp>
#include <eosiolib/action.hpp>
#include <eosiolib/contract.hpp>
#include <eosiolib/print.hpp>
#include <string>
#include <unordered_map>

using eosio::indexed_by;
using eosio::const_mem_fun;
using std::string;


class gdprcookie : public eosio::contract {

  public:
    using contract::contract;

    // -- add a new account to the list (is this needed?)
    // @abi action
    void add (const account_name account) {
      setting_index settings(_self, _self);

      auto itr = settings.find(account);
      eosio_assert(itr == settings.end(), "Account already exists!");

      settings.emplace(account, [&](auto& setting) {
        setting.account = account;
        setting.url_policies = std::vector<individualSettingKV>();
        setting.category_policies = std::vector<categorySettingKV>();
      });
    }

    // -- setting the whitelist for a site category (news, sports, ..)
    // @abi action
    void setcategory (const account_name account, 
                      string category, uint64_t permission_level) {
      setting_index settings(_self, _self);

      auto itr = settings.find(account);
      eosio_assert(itr != settings.end(), "Account does not exist");

      auto &setting_ = settings.get(account);

      // modify the setting variable category poliy (is 'modify' needed?)
      settings.modify(setting_, _self, [&](auto& setting_) {
        auto citr = setting_.category_itr(category);
        if (citr == setting_.category_policies.end()) {
          // item does not exist
          struct categorySettingKV cpolicy;
          cpolicy.build_struct(category, permission_level);
          setting_.category_policies.push_back(cpolicy);
        }
        else {
          // item exists, update it
          citr->category = category;
          citr->permission_level = permission_level;
          citr->timestamp = now();
        }

      });
    }

    // -- setting the URL whitelist for an account
    // @abi action
    void seturl (const account_name account, string url, uint64_t permission_level) {

      setting_index settings(_self, _self);

      auto itr = settings.find(account);
      eosio_assert(itr != settings.end(), "Account does not exist");

      // retreive the setting struct obj for this account
      auto &setting_ = settings.get(account);

      // modify the setting obect's url_policies vector based on `url`
      settings.modify(setting_, _self, [&](auto& setting_) {
        auto urlitr = setting_.url_itr(url);
        if ( urlitr == setting_.url_policies.end() ) {
          // item does not exist
          struct individualSettingKV urlpolicy;
          urlpolicy.build_struct(url, permission_level);
          setting_.url_policies.push_back(urlpolicy);
        }
        else {
          // item exists, now update it
          urlitr->url = url;
          urlitr->permission_level = permission_level;
          urlitr->timestamp = now();
        }
      });

    }

    // -- set the global actions for this account
    // @abi action
    void setglobal () {

    }

    // -- allow options in exchange for a bounty
    //

  private:

    struct individualSettingKV {
      // keyvalue model for url policies
      string url;
      uint64_t permission_level;
      uint64_t timestamp;

      void build_struct(string url, uint64_t permission_level) {
        this->url = url;
        this->permission_level = permission_level;
        this->timestamp = now();
      }
    };

    struct categorySettingKV {
      string category;
      uint64_t permission_level;
      uint64_t timestamp;

      void build_struct(string category, uint64_t permission_level) {
        this->category = category;
        this->permission_level = permission_level;
        this->timestamp = now();
      }
    };

    // @abi table settings i64
    struct setting {
      uint64_t account;

      // these maps stor the settings for individual urls and categories
      std::vector<individualSettingKV> url_policies;
      std::vector<categorySettingKV> category_policies;

      std::vector<individualSettingKV>::iterator url_itr (string url) {
        // -- just like find(), returns whether an iterator to the element if 
        // it exists or url_policies.end() if element does not exist
        for (auto iter=this->url_policies.begin(); iter != this->url_policies.end(); iter++) {
          if (iter->url == url) {
            return iter;
          }
        }
        return this->url_policies.end();
      }

      std::vector<categorySettingKV>::iterator category_itr (string category) {
        // -- similar to multi_index.find()
        // returns an iteror pointing to the element in category_policies, otherwise return category_poliies.end()
        for (auto iter=this->category_policies.begin(); iter != this->category_policies.end(); iter++) {
          if (iter->category == category) {
            return iter;
          }
        }
        return this->category_policies.end();
      }

      uint64_t primary_key() const { return account; }
    };

    typedef eosio::multi_index<N(settings), setting> setting_index;

    EOSLIB_SERIALIZE( setting, (account)(url_policies)(category_policies) )

};

EOSIO_ABI(gdprcookie, (add)(setcategory)(seturl)(setglobal))
