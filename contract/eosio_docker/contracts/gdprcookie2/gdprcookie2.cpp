#include <eosiolib/eosio.hpp>
#include <eosio.token/eosio.token.hpp>
#include <eosiolib/action.hpp>
#include <eosiolib/contract.hpp>
#include <eosiolib/print.hpp>
#include <string>
#include <unordered_map>

using eosio::indexed_by;
using eosio::const_mem_fun;
using std::string;


class gdprcookie : public eosio::contract {

  public:
    using contract::contract;

    // -- add a new account to the list (is this needed?)
    // @abi action
    void add (const account_name account) {
      // setting_index settings(_self, _self);

      // auto itr = settings.find(account);
      // eosio_assert(itr != settings.end(), "Account already exists!");

      // settings.emplace(account, [&](auto& setting) {
      //   setting.account = account;
      // });
    }

    // -- setting the whitelist for a site category (news, sports, ..)
    // @abi action
    void setcategory () {
      
    }

    // -- setting the URL whitelist for an account
    // @abi action
    void seturl () {
      
    }

    // -- set the global actions for this account
    // @abi action
    void setglobal () {
      
    }

    // -- allow options in exchange for a bounty
    //

  private:

    struct individualSettingKV {
      String url;
      int permission_level;
    }

    struct categorySettingKV {
      String category;
      int permission_level;
    }

    // @abi table settings i64
    struct setting {
      uint64_t account;

      // these maps stor the settings for individual urls and categories
      std::map<String, int> url_policies;
      std:map<String, int> category_policies;

      uint64_t primary_key() const { return account; }
    }

    typedef eosio::multi_index<N(settings), setting> setting_index;

    EOSLIB_SERIALIZE( SETTING, (account)(url_policies)(category_policies) )

};

EOSIO_ABI(gdprcookie, (add)(setcategory)(seturl)(setglobal))
